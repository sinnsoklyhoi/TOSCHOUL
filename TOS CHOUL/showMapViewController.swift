//
//  showMapViewController.swift
//  TOS CHOUL
//
//  Created by GIS on 8/8/18.
//  Copyright © 2018 GIS. All rights reserved.
//

import UIKit
import GoogleMaps

class showMapViewController: UIViewController,GMSMapViewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    
    var locationManager = CLLocationManager()
    var centerMapCoordinate = CLLocationCoordinate2D()
    var marker = GMSMarker()
    var locationString = String()
    var userLatAndLong = [[11.5750552905215,104.889334514737],[11.4952487942592,104.885341376066]]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mapView.isMyLocationEnabled = true
        self.mapView.userActivity?.delegate = self as? NSUserActivityDelegate
        self.mapView.delegate = self
        self.mapView.selectedMarker = marker
        mapView.settings.myLocationButton = true
        
        //User Location
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func showUserOnMap() {
    
        for user in userLatAndLong {
            
            let camera = GMSCameraPosition.camera(withLatitude: user[0] , longitude: user[1] , zoom: 15)
            let location = CLLocationCoordinate2D(latitude: user[0], longitude: user[1])
            
            print("location: \(location)")
            let marker = GMSMarker()
            marker.position = location
//            marker.snippet = user[1]
            marker.map = mapView
        }
    }
    
    
    //show lat and long when move the map
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        
        let lat = mapView.camera.target.latitude
        let long = mapView.camera.target.longitude
        centerMapCoordinate = CLLocationCoordinate2D(latitude: lat , longitude: long)
        self.placeMarkerOnCenter(position: centerMapCoordinate)
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(centerMapCoordinate) { response, error in
            
            let address = response?.firstResult()
            let lines = address?.lines
            print(lines?.joined(separator: " , "))
        }
        
        print(lat)
        print(long)
    }
    
    //show the lat and long when click a place on map
    private func mapView(_ mapView: GMSMapView, didTap coordinate: CLLocationCoordinate2D) {
        
        let lat = mapView.camera.target.latitude
        let long = mapView.camera.target.longitude
        centerMapCoordinate = CLLocationCoordinate2D(latitude: lat , longitude: long)
        self.placeMarkerOnCenter(position: centerMapCoordinate)
        
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(centerMapCoordinate) { response, error in
            
            let address = response?.firstResult()
            let lines = address?.lines
            self.locationString = lines!.joined(separator: " , ")
            print("location",self.locationString)
            print(lines?.joined(separator: " , "))
        }
        
        print(lat)
        print(long)
        
    }
    
    func placeMarkerOnCenter(position:CLLocationCoordinate2D) {
        //Create Marker when click
    }
    
    //Zoom Location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let location = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 15)
        self.mapView.animate(to: camera)
        showUserOnMap()
        self.locationManager.stopUpdatingLocation()
    }

}
